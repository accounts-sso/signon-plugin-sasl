#-----------------------------------------------------------------------------
# Common configuration for all projects.
#-----------------------------------------------------------------------------

QT		-= gui
CONFIG		+= link_pkgconfig

# we don't like warnings...
QMAKE_CXXFLAGS -= -Wall -Werror -Wno-write-strings
# Disable RTTI
QMAKE_CXXFLAGS += -fno-exceptions -fno-rtti

!defined(TOP_SRC_DIR, var) {
    TOP_SRC_DIR = $$PWD
    TOP_BUILD_DIR = $${TOP_SRC_DIR}/$(BUILD_DIR)
}

DEFINES		+= DEBUG_ENABLED
DEFINES		+= SIGNON_TRACE

#-----------------------------------------------------------------------------
# setup the installation prefix
#-----------------------------------------------------------------------------
INSTALL_PREFIX = /usr  # default installation prefix

# default prefix can be overriden by defining PREFIX when running qmake
isEmpty( PREFIX ) {
    message("====")
    message("==== NOTE: To override the installation path run: `qmake PREFIX=/custom/path'")
    message("==== (current installation path is `$${INSTALL_PREFIX}')")
} else {
    INSTALL_PREFIX = $${PREFIX}
    message("====")
    message("==== install prefix set to `$${INSTALL_PREFIX}'")
}

INSTALL_LIBDIR = $${INSTALL_PREFIX}/lib

# default library directory can be overriden by defining LIBDIR when
# running qmake
isEmpty( LIBDIR ) {
    message("====")
    message("==== NOTE: To override the library installation path run: `qmake LIBDIR=/custom/path'")
    message("==== (current installation path is `$${INSTALL_LIBDIR}')")
} else {
    INSTALL_LIBDIR = $${LIBDIR}
    message("====")
    message("==== library install path set to `$${INSTALL_LIBDIR}'")
}

# Default directory for signond extensions
_PLUGINS = $$system(pkg-config --variable=plugindir signon-plugins)
isEmpty(_PLUGINS) {
    error("plugin directory not available through pkg-config")
} else {
    SIGNON_PLUGINS_DIR = $$_PLUGINS
}

include(coverage.pri)
