/* -*- Mode: C++; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This file is part of signon
 *
 * Copyright (C) 2009-2010 Nokia Corporation.
 * Copyright (C) 2015 Canonical Ltd.
 *
 * Contact: Alberto Mardegan <alberto.mardegan@canonical.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */

#include "saslplugin.h"
#include "sasldata.h"
#include "saslplugin.cpp"

#include <QJsonDocument>
#include <QJsonObject>
#include <QSignalSpy>
#include <QTest>
#include <sasl/sasl.h>

using namespace SaslPluginNS;

namespace QTest {
template<> char *toString(const QVariantMap &map)
{
    QJsonDocument doc(QJsonObject::fromVariantMap(map));
    return qstrdup(doc.toJson(QJsonDocument::Compact).data());
}
} // QTest namespace

// sasl server implementation for response verification

class SaslServer {
public:
    SaslServer();
    ~SaslServer();
    int init(const QString &mech, QByteArray &challenge);
    int step(const QByteArray &response);

    int c;
    int errflag;
    int result;
    sasl_security_properties_t secprops;
    sasl_ssf_t extssf;
    char *options, *value;
    unsigned len, count;
    const char *data;
    int serverlast;
    sasl_ssf_t *ssf;

    const char *mech;
    const char *iplocal;
    const char *ipremote;
    const char *searchpath;
    const char *service;
    const char *localdomain;
    const char *userdomain;
    sasl_conn_t *conn;
};

int sasl_log(void *context, int priority, const char *message)
{
    Q_UNUSED(context);
    Q_UNUSED(priority);
    if (!message)
        return SASL_BADPARAM;

    TRACE() << message;
    return SASL_OK;
}

static sasl_callback_t callbacks[] = {
    {
        SASL_CB_LOG, (int(*)())(&sasl_log), NULL
    }, {
        SASL_CB_LIST_END, NULL, NULL
    }
};

void saslfail(int why, const char *what, const char *errstr)
{
    qDebug() << why << what << errstr;
}

#define SAMPLE_SEC_BUF_SIZE (2048)

char buf[SAMPLE_SEC_BUF_SIZE];

SaslServer::SaslServer()
{
    service = "sample";
    localdomain = "loc";
    userdomain = "realm";
    iplocal = "127.0.0.1";
    ipremote = "127.0.0.1";
    searchpath = ".";
    memset(&buf, 0L, SAMPLE_SEC_BUF_SIZE);
}

SaslServer::~SaslServer()
{
    sasl_dispose(&conn);
    //sasl_done(); //cannot be called, as plugin also runs this
}

int SaslServer::init(const QString &mech, QByteArray &challenge)
{
    const char *ext_authid = NULL;

    /* Init defaults... */
    memset(&secprops, 0L, sizeof(secprops));
    secprops.maxbufsize = SAMPLE_SEC_BUF_SIZE;
    secprops.max_ssf = UINT_MAX;

    result = sasl_server_init(callbacks, "sample");
    if (result != SASL_OK)
        saslfail(result, "Initializing libsasl", NULL);

    result = sasl_server_new(service,
                             localdomain,
                             userdomain,
                             iplocal,
                             ipremote,
                             NULL,
                             0,
                             &conn);
    if (result != SASL_OK)
        saslfail(result, "Allocating sasl connection state", NULL);

    if (!mech.isEmpty()) {
        data = strdup(mech.toLatin1().constData());
        len = (unsigned) strlen(data);
        count = 1;
    } else {
        result = sasl_listmech(conn,
                               ext_authid,
                               NULL,
                               " ",
                               NULL,
                               &data,
                               &len,
                               (int*)&count);
        if (result != SASL_OK)
            saslfail(result, "Generating client mechanism list", NULL);
    }

    if (!mech.isEmpty()) {
        free((void *)data);
    }

    strcpy(buf, mech.toLatin1().constData());
    len = strlen(buf);

    if (!mech.isEmpty() && strcasecmp(mech.toLatin1().constData(), buf))
    {
        qDebug("Client chose something other than the mandatory mechanism");
        return SASL_FAIL;
    }

    if (strlen(buf) < len) {
        data = buf + strlen(buf) + 1;
        len = len - strlen(buf) - 1;
    } else {
        data = NULL;
        len = 0;
    }
    result = sasl_server_start(conn, buf, data, len,
                               &data, &len);
    if (result != SASL_OK && result != SASL_CONTINUE) {
        saslfail(result, "Starting SASL negotiation",
                 sasl_errstring(result, NULL, NULL));
        return result;
    }
    if (len) {
        challenge = QByteArray(data, len);
    }
    return result;
}

int SaslServer::step(const QByteArray &response)
{
    if (!data) {
        qDebug("No data to send--something's wrong");
    }
    QByteArray resp = response;
    resp.replace('\0', ':');

    for (int i = 0; i < response.count(); i++) {
        buf[i] = (char)(response.constData()[i]);
    }
    len = response.count();
    buf[len] = 0;

    data = NULL;
    result = sasl_server_step(conn, buf, len,
                              &data, &len);
    if (result != SASL_OK && result != SASL_CONTINUE) {
        saslfail(result, "Performing SASL negotiation",
                 sasl_errstring(result, NULL, NULL));
        return result;
    }

    if (result == SASL_CONTINUE) {
        return result;
    }

    result = sasl_getprop(conn, SASL_USERNAME, (const void **)&data);

    return SASL_OK;
}
class SaslPluginTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void initTestCase();
    void cleanupTestCase();
    void init();
    void cleanup();

    //test cases
    void testPlugin();
    void testPluginType();
    void testPluginMechanisms();
    void testProcess();
    void testChallengePlain();
    void testChallengeDigestMd5();
    void testChallengeCramMd5();
    void testPluginsasl_callback();
    void testPluginsasl_get_realm();
    void testPluginsasl_get_secret();
    void testPluginsasl_log();
    void testPluginset_callbacks();
    void testParameters_data();
    void testParameters();
    //end test cases

private:
    SaslPlugin *m_testPlugin;
};

void SaslPluginTest::initTestCase()
{
    qRegisterMetaType<SignOn::SessionData>();
}

void SaslPluginTest::cleanupTestCase()
{
    sasl_done();
}

void SaslPluginTest::init()
{
    m_testPlugin = new SaslPlugin();
}

void SaslPluginTest::cleanup()
{
    delete m_testPlugin;
    m_testPlugin = 0;
}

void SaslPluginTest::testPlugin()
{
    QVERIFY(m_testPlugin);
}

void SaslPluginTest::testPluginType()
{
    QCOMPARE(m_testPlugin->type(), QString("sasl"));
}

void SaslPluginTest::testPluginMechanisms()
{
    QStringList mechs = m_testPlugin->mechanisms();
    QVERIFY(!mechs.isEmpty());
    QVERIFY(mechs.contains(QString("PLAIN")));
}

void SaslPluginTest::testProcess()
{
    SaslData info;

    QSignalSpy resultSpy(m_testPlugin,
                         SIGNAL(result(const SignOn::SessionData&)));
    QSignalSpy errorSpy(m_testPlugin, SIGNAL(error(const SignOn::Error&)));

    //try without username
    m_testPlugin->process(info, QString("ANONYMOUS"));
    QCOMPARE(errorSpy.count(), 1);
    QCOMPARE(errorSpy.at(0).at(0).value<SignOn::Error>().type(),
             int(SignOn::Error::MissingData));
    QCOMPARE(resultSpy.count(), 0);

    //try without mechanism
    errorSpy.clear();
    info.setUserName(QString("test"));
    m_testPlugin->process(info);
    QCOMPARE(errorSpy.count(), 1);
    QCOMPARE(errorSpy.at(0).at(0).value<SignOn::Error>().type(),
             int(SignOn::Error::MechanismNotAvailable));
    QCOMPARE(resultSpy.count(), 0);
}

void SaslPluginTest::testChallengePlain()
{
    SaslData info;

    QSignalSpy resultSpy(m_testPlugin,
                         SIGNAL(result(const SignOn::SessionData&)));
    QSignalSpy errorSpy(m_testPlugin, SIGNAL(error(const SignOn::Error&)));

    info.setUserName(QString("idmtestuser"));
    info.setSecret(QString("abc123"));
    info.setAuthName(QString("authn"));

    //give challenge to plugin
    m_testPlugin->process(info, QString("PLAIN"));

    //test response here
    QCOMPARE(errorSpy.count(), 0);
    QCOMPARE(resultSpy.count(), 1);
    SaslData result =
        resultSpy.at(0).at(0).value<SignOn::SessionData>().data<SaslData>();
    QByteArray token=result.Response();
    token.replace('\0',':');
    QCOMPARE(result.Response(), QByteArray("idmtestuser\0authn\0abc123",11+5+6+2));

    //create connection to server to get initial challenge
    SaslServer *server = new SaslServer();
    QByteArray reply;
    server->init(QString("PLAIN"), reply);

    //check that authentication server is happy about answer
    int retval = server->step(result.Response());
    QCOMPARE(retval, SASL_NOUSER);

    delete server;
}

void SaslPluginTest::testChallengeDigestMd5()
{
    SaslData info;

    QSignalSpy resultSpy(m_testPlugin,
                         SIGNAL(result(const SignOn::SessionData&)));
    QSignalSpy errorSpy(m_testPlugin, SIGNAL(error(const SignOn::Error&)));

    info.setUserName(QString("idmtestuser"));
    info.setSecret(QString("abc123"));
    info.setAuthName(QString("authn"));
    info.setRealm(QString("realm"));
    info.setService(QByteArray("sample"));

    //create connection to server to get initial challenge
    SaslServer *server = new SaslServer();

    QByteArray challenge;
    server->init(QString("DIGEST-MD5"), challenge);
    info.setChallenge(challenge);

    //give challenge to plugin
    m_testPlugin->process(info, QString("DIGEST-MD5"));

    //test response here
    QCOMPARE(errorSpy.count(), 0);
    QCOMPARE(resultSpy.count(), 1);
    SaslData result =
        resultSpy.at(0).at(0).value<SignOn::SessionData>().data<SaslData>();

    int retval = server->step(result.Response());
    QCOMPARE(retval, SASL_NOUSER);

    delete server;
}

void SaslPluginTest::testChallengeCramMd5()
{
    SaslData info;

    QSignalSpy resultSpy(m_testPlugin,
                         SIGNAL(result(const SignOn::SessionData&)));
    QSignalSpy errorSpy(m_testPlugin, SIGNAL(error(const SignOn::Error&)));

    info.setUserName(QString("idmtestuser"));
    info.setSecret(QString("abc123"));
    info.setAuthName(QString("authn"));
    info.setRealm(QString("realm"));
    info.setService(QByteArray("sample"));

    //create connection to server to get initial challenge
    SaslServer *server = new SaslServer();

    QByteArray challenge;
    int serret = server->init(QString("CRAM-MD5"), challenge); //fails sometimes
    if (serret != SASL_OK) {
        QSKIP("sasl server init for CRAM-MD5 failed", SkipSingle);
    }

    info.setChallenge(challenge);

    //give challenge to plugin
    m_testPlugin->process(info, QString("CRAM-MD5"));

    //test response here
    QCOMPARE(errorSpy.count(), 0);
    QCOMPARE(resultSpy.count(), 1);
    SaslData result =
        resultSpy.at(0).at(0).value<SignOn::SessionData>().data<SaslData>();

    int retval = server->step(result.Response()); //fails, check server impl.
    QCOMPARE(retval, SASL_NOUSER);

    delete server;
}

//private funcs

void SaslPluginTest::testPluginsasl_callback()
{
    int ret;
    const char *res;
    unsigned len;
    ret = m_testPlugin->sasl_callback(NULL, 0, &res, &len);
    QCOMPARE(ret, SASL_BADPARAM);
    ret = m_testPlugin->sasl_callback(m_testPlugin, 0, NULL, &len);
    QCOMPARE(ret, SASL_BADPARAM);

    m_testPlugin->d->m_input.setUserName(QString("user"));

    ret = m_testPlugin->sasl_callback(m_testPlugin, SASL_CB_USER, &res, &len);
    QCOMPARE(ret, SASL_OK);
    QCOMPARE(QByteArray(res), QByteArray("user"));
    QCOMPARE(len, uint(4));

    m_testPlugin->d->m_input.setAuthName(QString("auth"));

    ret = m_testPlugin->sasl_callback(m_testPlugin, SASL_CB_AUTHNAME, &res, &len);
    QCOMPARE(ret, SASL_OK);
    QCOMPARE(QByteArray(res), QByteArray("auth"));
    QCOMPARE(len, uint(4));

    ret = m_testPlugin->sasl_callback(m_testPlugin, SASL_CB_LANGUAGE, &res, &len);
    QCOMPARE(ret, SASL_OK);
    QVERIFY(!res);
    QCOMPARE(len, uint(0));

    ret = m_testPlugin->sasl_callback(m_testPlugin, 45643, &res, &len);
    QCOMPARE(ret, SASL_BADPARAM);
}

void SaslPluginTest::testPluginsasl_get_realm()
{
    int ret;
    const char *res;

    ret = m_testPlugin->sasl_get_realm(NULL, 0, NULL, NULL);
    QCOMPARE(ret, SASL_FAIL);
    ret = m_testPlugin->sasl_get_realm(NULL, SASL_CB_GETREALM, NULL, NULL);
    QCOMPARE(ret, SASL_BADPARAM);
    ret = m_testPlugin->sasl_get_realm(m_testPlugin, SASL_CB_GETREALM, NULL, NULL);
    QCOMPARE(ret, SASL_BADPARAM);

    ret = m_testPlugin->sasl_get_realm(m_testPlugin, SASL_CB_GETREALM, NULL, &res);
    QCOMPARE(ret, SASL_OK);

    m_testPlugin->d->m_input.setRealm(QString("real"));

    ret = m_testPlugin->sasl_get_realm(m_testPlugin, SASL_CB_GETREALM, NULL, &res);
    QCOMPARE(ret, SASL_OK);
    QCOMPARE(QByteArray(res), QByteArray("real"));
}

void SaslPluginTest::testPluginsasl_get_secret()
{
    int ret;
    sasl_secret_t *secret = 0;

    ret = m_testPlugin->sasl_get_secret(m_testPlugin->d->m_conn, NULL,
                                        0, &secret);
    QCOMPARE(ret, SASL_BADPARAM);

    m_testPlugin->d->m_input.setSecret(QString("password"));

    ret = m_testPlugin->sasl_get_secret(m_testPlugin->d->m_conn, m_testPlugin,
                                        SASL_CB_PASS, NULL);
    QCOMPARE(ret, SASL_BADPARAM);
    ret = m_testPlugin->sasl_get_secret(m_testPlugin->d->m_conn, m_testPlugin,
                                        SASL_CB_PASS, &secret);
    QCOMPARE(ret, SASL_OK);
    QCOMPARE(QByteArray("password"),
             QByteArray((const char*)secret->data, (int)secret->len));
}

void SaslPluginTest::testPluginsasl_log()
{
    int ret;

    ret = m_testPlugin->sasl_log(m_testPlugin, 0, NULL);
    QCOMPARE(ret, SASL_BADPARAM);

    ret = m_testPlugin->sasl_log(m_testPlugin, 0, "test debug");
    QCOMPARE(ret, SASL_OK);
}

void SaslPluginTest::testPluginset_callbacks()
{
    m_testPlugin->set_callbacks();
    sasl_callback_t *callback;
    callback = m_testPlugin->d->m_callbacks;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_LOG);
    QVERIFY(callback->context == m_testPlugin);
    ++callback;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_USER);
    QVERIFY(callback->context == m_testPlugin);
    ++callback;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_AUTHNAME);
    QVERIFY(callback->context == m_testPlugin);
    ++callback;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_PASS);
    QVERIFY(callback->context == m_testPlugin);
    ++callback;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_GETREALM);
    QVERIFY(callback->context == m_testPlugin);
    ++callback;
    QVERIFY(callback != NULL);
    QVERIFY(callback->id == SASL_CB_LIST_END);
    QVERIFY(callback->context == NULL);
}

void SaslPluginTest::testParameters_data()
{
    qDebug() << "Called!";
    QTest::addColumn<QVariantMap>("parameters");
    QTest::addColumn<QString>("mechanism");
    QTest::addColumn<bool>("isValid");
    QTest::addColumn<QVariantMap>("expectedParams");

    QVariantMap parameters;
    QVariantMap expectedParams;
    QTest::newRow("empty") <<
        parameters << QString() << false << expectedParams;

    parameters["UserName"] = "user";
    expectedParams["AuthName"] = "user";
    expectedParams["UserName"] = "user";
    expectedParams["Service"] = "default";
    expectedParams["Fqdn"] = "default";
    expectedParams["IpLocal"] = "127.0.0.1";
    expectedParams["IpRemote"] = "127.0.0.1";
    expectedParams["MechList"] = "PLAIN";
    QTest::newRow("only user") <<
        parameters <<
        "PLAIN" <<
        true <<
        expectedParams;
    parameters.clear();
    expectedParams.clear();

    parameters["UserName"] = "user";
    parameters["MechList"] = "MECH1 MECH2";
    expectedParams["AuthName"] = "user";
    expectedParams["UserName"] = "user";
    expectedParams["Service"] = "default";
    expectedParams["Fqdn"] = "default";
    expectedParams["IpLocal"] = "127.0.0.1";
    expectedParams["IpRemote"] = "127.0.0.1";
    expectedParams["MechList"] = "MECH1 MECH2";
    QTest::newRow("with mechlist") <<
        parameters <<
        "PLAIN" <<
        true <<
        expectedParams;
}

void SaslPluginTest::testParameters()
{
    QFETCH(QVariantMap, parameters);
    QFETCH(QString, mechanism);
    QFETCH(bool, isValid);
    QFETCH(QVariantMap, expectedParams);

    SaslData input(parameters);
    bool ret =
        m_testPlugin->check_and_fix_parameters(input, mechanism.toUtf8());
    QCOMPARE(ret, isValid);

    QCOMPARE(input.toMap(), expectedParams);
}

QTEST_MAIN(SaslPluginTest)
#include "tst_plugin.moc"
