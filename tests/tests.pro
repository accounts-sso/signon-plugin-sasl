include( ../common-project-config.pri )
include( ../common-vars.pri )

TARGET = tst_plugin

QT += \
    core \
    testlib
QT -= gui
CONFIG += \
    link_pkgconfig \

SOURCES += tst_plugin.cpp

HEADERS += \
    $${TOP_SRC_DIR}/src/saslplugin.h

INCLUDEPATH += . \
    $${TOP_SRC_DIR}/src

PKGCONFIG += \
    libsasl2 \
    libsignon-qt5 \
    signon-plugins

check.depends = $$TARGET
check.commands = ./$$TARGET
QMAKE_EXTRA_TARGETS += check
