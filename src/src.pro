include( ../common-project-config.pri )
include( ../common-vars.pri )
TEMPLATE = lib
TARGET = saslplugin
QT += core
QT -= gui
CONFIG += plugin \
	build_all \
	warn_on \
	link_pkgconfig
DEFINES += SIGNON_PLUGIN_TRACE

public_headers += \
    sasldata.h
private_headers += \
    saslplugin.h
HEADERS += $$public_headers $$private_headers

SOURCES += saslplugin.cpp

PKGCONFIG += \
    libsasl2 \
    libsignon-qt5 \
    signon-plugins

headers.files = $$public_headers

pkgconfig.files = ../signon-saslplugin.pc

include( ../common-installs-config.pri )

